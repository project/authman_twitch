<?php

declare(strict_types = 1);

namespace Drupal\authman_twitch\Forms;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Form for configuring Twitch plugins.
 *
 * @property \Drupal\authman_twitch\Plugin\AuthmanOauth\AuthmanTwitch $plugin
 */
class AuthmanTwitchPluginForm extends PluginFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $configuration = $this->plugin->getConfiguration();

    $options = [];
    $options['analytics:read:extensions'] = [
      $this->t('Analytics: extensions'),
      $this->t('View analytics data for your extensions.'),
    ];
    $options['analytics:read:games'] = [
      $this->t('Analytics: games'),
      $this->t('View analytics data for your games.'),
    ];
    $options['bits:read'] = [
      $this->t('Bits: view'),
      $this->t('View Bits information for your channel.'),
    ];
    $options['channel:edit:commercial'] = [
      $this->t('Channel: run commercials'),
      $this->t('Run commercials on a channel.'),
    ];
    $options['channel:manage:broadcast'] = [
      $this->t('Channel: manage broadcast'),
      $this->t('Manage your channel’s broadcast configuration, including updating channel configuration and managing stream markers and stream tags.'),
    ];
    $options['channel:manage:extensions'] = [
      $this->t('Channel: manage extensions'),
      $this->t('Manage your channel’s extension configuration, including activating extensions.'),
    ];
    $options['channel:read:hype_train'] = [
      $this->t('Channel: hype train'),
      $this->t('Gets the most recent hype train on a channel.'),
    ];
    $options['channel:read:stream_key'] = [
      $this->t('Channel: stream key'),
      $this->t('Read an authorized user’s stream key.'),
    ];
    $options['channel:read:subscriptions'] = [
      $this->t('Channel: subscribers'),
      $this->t('Get a list of all subscribers to your channel and check if a user is subscribed to your channel'),
    ];
    $options['clips:edit'] = [
      $this->t('Clips: manage'),
      $this->t('Manage a clip object.'),
    ];
    $options['user:edit'] = [
      $this->t('User: edit'),
      $this->t('Manage a user object.'),
    ];
    $options['user:edit:follows'] = [
      $this->t('User: edit followers'),
      $this->t('Edit your follows.'),
    ];
    $options['user:read:broadcast'] = [
      $this->t('User: read broadcast information'),
      $this->t('View your broadcasting configuration, including extension configurations.'),
    ];
    $options['user:read:email'] = [
      $this->t('User: read email address'),
      $this->t('Read an authorized user’s email address.'),
    ];
    $options['user_read'] = [
      $this->t('User: read'),
      $this->t('Read user information (required for resource owner testing)'),
    ];

    $form['scopes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Scopes'),
      '#description' => $this->t('Request scopes.'),
      '#options' => array_combine(array_keys($options), array_column($options, 0)),
      '#default_value' => $configuration['scopes'],
    ];
    foreach ($options as $key => [1 => $description]) {
      $form['scopes'][$key]['#description'] = $description;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $config = [
      'scopes' => array_keys(array_filter($form_state->getValue('scopes', []))),
    ];
    $this->plugin->setConfiguration($config);
  }

}

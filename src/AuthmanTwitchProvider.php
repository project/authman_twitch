<?php

declare(strict_types = 1);

namespace Drupal\authman_twitch;

use Depotwarehouse\OAuth2\Client\Twitch\Provider\Twitch;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Wraps Twitch provider so return values are valid.
 *
 * Remove after https://github.com/tpavlek/oauth2-twitch/pull/17 is committed.
 *
 * This class should not be extended as it is considered a temporary fix.
 */
final class AuthmanTwitchProvider extends Twitch {

  /**
   * {@inheritdoc}
   */
  public $authorizationDomain = 'https://id.twitch.tv';

  /**
   * {@inheritdoc}
   */
  public $authorizationPath = '/oauth2';

  /**
   * {@inheritdoc}
   */
  public $resourceDomain = 'https://api.twitch.tv';

  /**
   * {@inheritdoc}
   */
  public $resourcePath = '/helix';

  /**
   * {@inheritdoc}
   */
  public function getBaseAuthorizationUrl() {
    return $this->authorizationDomain . $this->authorizationPath . '/authorize';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAccessTokenUrl(array $params) {
    return $this->authorizationDomain . $this->authorizationPath . '/token';
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceOwnerDetailsUrl(AccessToken $token) {
    return $this->resourceDomain . $this->resourcePath . '/users?oauth_token=' . $token->getToken();
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthenticatedUrlForEndpoint($endpoint, AccessToken $token) {
    return $this->authorizationDomain . $endpoint . '?oauth_token=' . $token->getToken();
  }

  /**
   * {@inheritdoc}
   */
  public function getUrlForEndpoint($endpoint) {
    return $this->authorizationDomain . $endpoint;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultHeaders() {
    return ['Client-ID' => $this->clientId];
  }

  /**
   * {@inheritdoc}
   */
  protected function createResourceOwner(array $response, AccessToken $token) {
    return new AuthmanTwitchUser($response['data'][0]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthorizationHeaders($token = NULL) {
    return $token ? [
      'Authorization' => 'Bearer ' . $token->getToken(),
    ] : [];
  }

}

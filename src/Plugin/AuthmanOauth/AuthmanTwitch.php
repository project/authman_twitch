<?php

declare(strict_types = 1);

namespace Drupal\authman_twitch\Plugin\AuthmanOauth;

use Depotwarehouse\OAuth2\Client\Twitch\Entity\TwitchUser;
use Drupal\authman\AuthmanInstance\AuthmanOauthInstance;
use Drupal\authman\AuthmanInstance\AuthmanOauthInstanceInterface;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginBase;
use Drupal\authman\Plugin\AuthmanOauth\AuthmanOauthPluginResourceOwnerInterface;
use Drupal\authman\Plugin\KeyType\OauthClientKeyType;
use Drupal\authman_twitch\AuthmanTwitchProvider;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\key\KeyInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Twitch OAuth provider.
 *
 * @AuthmanOauth(
 *   id = "authman_twitch",
 *   label = @Translation("Twitch"),
 *   grant_types = {
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_AUTHORIZATION_CODE,
 *     \Drupal\authman\Entity\AuthmanAuthInterface::GRANT_REFRESH_TOKEN,
 *   },
 *   forms = {
 *     "configure" = "\Drupal\authman_twitch\Forms\AuthmanTwitchPluginForm",
 *   }
 * )
 *
 * @internal
 */
class AuthmanTwitch extends AuthmanOauthPluginBase implements ConfigurableInterface, ContainerFactoryPluginInterface, AuthmanOauthPluginResourceOwnerInterface {

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'scopes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance(array $providerOptions, string $grantType, KeyInterface $clientKey): AuthmanOauthInstanceInterface {
    $keyType = $clientKey->getKeyType();
    assert($keyType instanceof OauthClientKeyType);
    $provider = $this->createProvider($providerOptions, $clientKey);
    return new AuthmanOauthInstance($provider, $grantType);
  }

  /**
   * {@inheritdoc}
   */
  protected function createProvider(array $providerOptions, KeyInterface $clientKey): AbstractProvider {
    // Include 'scopes' only if its not empty.
    $scopes = $this->getConfiguration()['scopes'] ?? [];
    if (count($scopes) > 0) {
      // Combine custom scopes.
      $providerOptions['scopes'] = ($providerOptions['scopes'] ?? []) + $scopes;
    }

    $values = $clientKey->getKeyValues();
    $provider = new AuthmanTwitchProvider([
      'clientId' => $values['client_id'],
      'clientSecret' => $values['client_secret'],
    ] + $providerOptions);
    $provider->setHttpClient($this->httpClient);
    return $provider;
  }

  /**
   * {@inheritdoc}
   */
  public function renderResourceOwner(ResourceOwnerInterface $resourceOwner): array {
    assert($resourceOwner instanceof TwitchUser);
    $build = [];
    $build['owner'] = [
      '#theme' => 'authman_twitch_resource_owner',
      'name' => $resourceOwner->getDisplayName(),
      'email' => $resourceOwner->getEmail(),
      'url' => 'https://www.twitch.tv/' . $resourceOwner->getUsername(),
      'avatar_url' => $resourceOwner->getLogo(),
    ];
    return $build;
  }

}

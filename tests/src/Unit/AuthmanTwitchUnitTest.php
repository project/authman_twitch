<?php

declare(strict_types = 1);

namespace Drupal\Tests\authman_twitch\Unit;

use Drupal\authman\AuthmanInstance\AuthmanOauthFactory;
use Drupal\authman\AuthmanInstance\AuthmanOauthInstance;
use Drupal\authman\Controller\AuthmanOauthAuthorizationCodeController;
use Drupal\authman\Entity\AuthmanAuthInterface;
use Drupal\authman\EntityHandlers\AuthmanAuthStorage;
use Drupal\authman\Plugin\KeyType\OauthClientKeyType;
use Drupal\authman_twitch\Plugin\AuthmanOauth\AuthmanTwitch;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\Core\Utility\UnroutedUrlAssemblerInterface;
use Drupal\key\KeyInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\authman\AuthmanInstance\AuthmanOauthInstanceInterface;

/**
 * Authman Twitch.
 *
 * @group authman_twitch
 */
final class AuthmanTwitchUnitTest extends UnitTestCase {

  /**
   * URL generator for testing.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a different container for global.
    $container = new ContainerBuilder();
    $urlAssembler = $this->createMock(UnroutedUrlAssemblerInterface::class);
    $urlAssembler->expects($this->any())
      ->method('assemble')
      ->will($this->returnArgument(0));
    $container->set('unrouted_url_assembler', $urlAssembler);
    $this->urlGenerator = $this->createMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $container->set('url_generator', $this->urlGenerator);
    \Drupal::setContainer($container);
  }

  /**
   * Tests authorization code URL generation.
   */
  public function testAuthorizationCodeUrl() {
    $plugin = $this->createPlugin($this->createMock(ClientInterface::class));

    $providerOptions = [
      'redirectUri' => 'http://example.com/receive/twitchh',
    ];
    $grantType = 'authorization_code';

    $keyType = $this->createMock(OauthClientKeyType::class);

    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->once())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->once())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);

    /** @var \Drupal\authman\AuthmanInstance\AuthmanOauthInstance $instance */
    $instance = $plugin->createInstance($providerOptions, $grantType, $clientKey);
    $this->assertInstanceOf(AuthmanOauthInstance::class, $instance);

    // State generation happens in AbstractProvider::getRandomState and happens
    // only after the method is called.
    $url = $instance->authorizationCodeUrl();
    $randomState = $instance->getProvider()->getState();
    $this->assertEquals(32, strlen($randomState));
    $this->assertEquals('https://id.twitch.tv/oauth2/authorize?state=' . $randomState . '&scope=user_read&response_type=code&approval_prompt=auto&redirect_uri=http%3A%2F%2Fexample.com%2Freceive%2Ftwitchh&client_id=test_client_id', $url->toString());
  }

  /**
   * Tests authorization token flow.
   *
   * Simulates a user agent hitting the receive route with query args redirected
   * from Twitch. This will capture the request from us to Twitch.
   */
  public function testAuthorizationToken() {
    $mockHandler = new MockHandler();
    $handlerStack = HandlerStack::create($mockHandler);
    $httpClient = new Client(['handler' => $handlerStack]);
    $plugin = $this->createPlugin($httpClient);

    $mockHandler->append(
      new GuzzleResponse(
        200,
        ['Content-Type' => 'application/json'],
        '{
                "access_token": "a_access_token",
                "expires_in": 14569,
                "refresh_token": "a_refresh_token",
                "scope": [
                  "user_read"
                ],
                "token_type": "bearer"
              }'
      ),
    );

    // Keys.
    $keyType = $this->createMock(OauthClientKeyType::class);
    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->once())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->any())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);
    $accessTokenKey = $this->createMock(KeyInterface::class);
    $accessTokenKey->expects($this->once())
      ->method('setKeyValue')
      ->with($this->callback(function ($arg1) {
        // 'expires' is not mockable since it uses time(), so we use this
        // callback to check only a few values.
        return (
          $arg1['access_token'] === 'a_access_token'
          && $arg1['refresh_token'] === 'a_refresh_token'
          && $arg1['token_type'] === 'bearer');
      }))
      ->willReturnSelf();

    $providerOptions = [
      'redirectUri' => 'http://example.com/receive/twitchh',
    ];

    /** @var \Drupal\authman\AuthmanInstance\AuthmanOauthInstance $instance */
    $instance = $plugin->createInstance($providerOptions, 'authorization_code', $clientKey);

    $messageUrl = $this->createMock(Url::class);
    $messageUrl->method('access')->willReturn(TRUE);
    $accessTokenKey->expects($this->once())
      ->method('toUrl')
      ->withAnyParameters()
      ->willReturn($messageUrl);

    $keyConfigStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $keyConfigStorage->expects($this->exactly(2))
      ->method('load')
      ->with('access_token_key_id')
      ->willReturn($accessTokenKey);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $entityTypeManager->expects($this->atLeastOnce())
      ->method('getStorage')
      ->with('key')
      ->willReturn($keyConfigStorage);

    $privateStoreFactory = $this->createMock(PrivateTempStoreFactory::class);
    $authmanOauthFactory = $this->createMock(AuthmanOauthFactory::class);
    $authmanOauthFactory->expects($this->once())
      ->method('get')
      ->with('1337')
      ->willReturn($instance);
    $messenger = $this->createMock(MessengerInterface::class);
    $currentUser = $this->createMock(AccountProxyInterface::class);

    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $entityTypeManager);
    $container->set('tempstore.private', $privateStoreFactory);
    $container->set('authman.oauth', $authmanOauthFactory);
    $container->set('messenger', $messenger);
    $container->set('current_user', $currentUser);
    $container->set('string_translation', $this->getStringTranslationStub());
    $codeController = AuthmanOauthAuthorizationCodeController::create($container);

    $request = new Request();
    $request->query->set('code', 'abc123');
    $authmanConfig = $this->createMock(AuthmanAuthInterface::class);
    $authmanConfig->expects($this->any())
      ->method('id')
      ->willReturn('1337');
    $authmanConfig->expects($this->once())
      ->method('getAccessTokenKeyId')
      ->willReturn('access_token_key_id');

    $this->urlGenerator->expects($this->at(0))
      ->method('generateFromRoute')
      ->with('entity.authman_auth.information')
      ->willReturn('/authman/instance/1337');

    /** @var \Symfony\Component\HttpFoundation\RedirectResponse $response */
    $response = $codeController->receive($request, $authmanConfig);
    $this->assertInstanceOf(RedirectResponse::class, $response);
    $this->assertEquals('/authman/instance/1337', $response->getTargetUrl());
  }

  /**
   * Tests authenticated request.
   */
  public function testAuthenticatedRequest() {
    $mockHandler = new MockHandler();
    $handlerStack = HandlerStack::create($mockHandler);
    $httpClient = new Client(['handler' => $handlerStack]);
    $plugin = $this->createPlugin($httpClient);

    $mockHandler->append(
      new GuzzleResponse(
        200,
        ['Content-Type' => 'application/json'],
        '{
          "total": 2,
          "data": [
            {
              "from_id": "546208461",
              "from_name": "TheDongerDragons",
              "to_id": "22484632",
              "to_name": "forsen",
              "followed_at": "2018-10-26T12:52:31Z"
            },
            {
              "from_id": "546208461",
              "from_name": "TheDongerDragons",
              "to_id": "30890872",
              "to_name": "cjayride",
              "followed_at": "2014-10-11T12:52:11Z"
            }
          ],
          "pagination": {
            "cursor": "eyJiIjpudWxsLCJhIjp7IkN1cnNvciI6JmV5SjBjQ0k1SW2WelpYSTZOREEzT1RNd01EazZabTlzYkc5M2N5SXNJblJ6SWpvaWRYTmxjam95TWpVNE1EQXhOeUlzSW1sd0lqb2lkWE5sY2pvME1EYzVNekF3T1RwbWIyeHNiM2R6SWl3aWFYTWlPaUl4TlRRMk5qZzJPRE14TVRZd056RTRPRGMySW4wPSJ9fQ"
          }
        }'
      ),
    );

    $this->urlGenerator->expects($this->at(0))
      ->method('generateFromRoute')
      ->with('authman.authorization_code.receive')
      ->willReturn((new GeneratedUrl())->setGeneratedUrl('/authman/receive/foo'));

    $keyType = $this->createMock(OauthClientKeyType::class);
    $authmanConfigId = 'foo';

    $authmanConfig = $this->createMock(AuthmanAuthInterface::class);
    $authmanConfig->expects($this->any())
      ->method('id')
      ->willReturn($authmanConfigId);
    $authmanConfig->expects($this->any())
      ->method('getClientKeyId')
      ->willReturn('client_key_id');
    $authmanConfig->expects($this->any())
      ->method('getAccessTokenKeyId')
      ->willReturn('access_token_key_id');
    $authmanConfig->expects($this->any())
      ->method('getPlugin')
      ->willReturn($plugin);
    $authmanConfig->expects($this->any())
      ->method('getGrantType')
      ->willReturn(AuthmanAuthInterface::GRANT_AUTHORIZATION_CODE);

    $clientKey = $this->createMock(KeyInterface::class);
    $clientKey->expects($this->any())
      ->method('getKeyType')
      ->willReturn($keyType);
    $clientKey->expects($this->any())
      ->method('getKeyValues')
      ->willReturn([
        'client_id' => 'test_client_id',
        'client_secret' => 'test_client_secret',
      ]);
    $accessTokenKey = $this->createMock(KeyInterface::class);
    $accessTokenKey->expects($this->any())
      ->method('getKeyType')
      ->willReturn($keyType);
    $accessTokenKey->expects($this->once())
      ->method('getKeyValues')
      ->willReturn([
        'access_token' => 'a179characterrandomtoken',
        'refresh_token' => 'a179characterrandomtoken',
        'token_type' => 'Bearer',
        // Fake expiration.
        'expires' => time() + 3600,
      ]);

    $authmanAuthStorage = $this->createMock(AuthmanAuthStorage::class);
    $authmanAuthStorage->expects($this->any())
      ->method('load')
      ->with('foo')
      ->willReturn($authmanConfig);

    $keyStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $keyStorage->expects($this->at(0))
      ->method('load')
      ->with('client_key_id')
      ->willReturn($clientKey);
    $keyStorage->expects($this->at(1))
      ->method('load')
      ->with('access_token_key_id')
      ->willReturn($accessTokenKey);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturnMap([
        ['key', $keyStorage],
        ['authman_auth', $authmanAuthStorage],
      ]);

    $authmanOauthFactory = new AuthmanOauthFactory($entityTypeManager);
    $authmanInstance = $authmanOauthFactory->get($authmanConfigId);
    $this->assertInstanceOf(AuthmanOauthInstanceInterface::class, $authmanInstance);

    $response = $authmanInstance->authenticatedRequest('GET', 'https://api.twitch.tv/helix/users/follows?from_id=546208461');
    $this->assertEquals('application/json', $response->getHeader('Content-Type')[0]);
  }

  /**
   * Creates a plugin for testing.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   *
   * @return \Drupal\authman_twitch\Plugin\AuthmanOauth\AuthmanTwitch
   *   The plugin for testing.
   */
  protected function createPlugin(ClientInterface $httpClient): AuthmanTwitch {
    $container = $this->createMock(ContainerInterface::class);
    $container->expects($this->once())
      ->method('get')
      ->with('http_client')
      ->willReturn($httpClient);

    $plugin = AuthmanTwitch::create($container, [], '', []);
    $plugin->setConfiguration([
      'scopes' => [
        'user_read',
      ],
    ]);

    return $plugin;
  }

}
